package com.inhouse.marked.domain.repository;

import com.inhouse.marked.domain.Product;

import java.util.List;
import java.util.Optional;

public interface ProductRepostory {
    List<Product> getAll();
    Optional<List<Product>> getByCategory(int categoryId);
    Optional<List<Product>> getScarsePorducts(int quantity);
    Optional<Product> getProduct(int productId);
    Product save(Product product);
    void delete(int productId);
}
