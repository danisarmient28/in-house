package com.inhouse.marked.domain.service;

import com.inhouse.marked.domain.Product;
import com.inhouse.marked.domain.repository.ProductRepostory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductServices {
    @Autowired
    private ProductRepostory productRepostory;

    public List<Product> getAll() {
        return productRepostory.getAll();
    }

    public Optional<Product> getProduct(int productId) {
        return productRepostory.getProduct(productId);
    }

    public Optional<List<Product>> getByCategory(int categoryId) {
        return productRepostory.getByCategory(categoryId);
    }
    public Product save(Product product) {
        return productRepostory.save(product);
    }

    public boolean delete(int productid){
        return getProduct(productid).map(product -> {
            productRepostory.delete(productid);
            return true;
        }).orElse(false);
    }
}