package com.inhouse.marked;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InHouseMarkedApplication {

	public static void main(String[] args) {
		SpringApplication.run(InHouseMarkedApplication.class, args);
	}

}
